
all: dual_quad_bench constant.frag.spv constant.vert.spv

dual_quad_bench: src/main.cpp
	g++ -o dual_quad_bench src/main.cpp -lvulkan


constant.frag.spv: src/constant.frag
	glslc -o constant.frag.spv src/constant.frag -O

constant.vert.spv: src/constant.vert
	glslc -o constant.vert.spv src/constant.vert -O
