#include <vulkan/vulkan.h>

#include <cstdint>
#include <fstream>
#include <stdio.h>
#include <unistd.h>
#include <vector>

struct VulkanTest {
public:
  VulkanTest();
  virtual ~VulkanTest();

  VkInstance vk_instance() { return vk_instance_; }
  VkPhysicalDevice vk_physical_device() { return vk_physical_device_; }
  VkDevice vk_device() { return vk_device_; }
  VkQueue vk_queue() { return vk_queue_; }
  VkCommandPool vk_cmd_pool() { return vk_cmd_pool_; }
  const VkPhysicalDeviceProperties &pdev_props() const { return pdev_props_; }

private:
  VkInstance vk_instance_;
  VkPhysicalDevice vk_physical_device_;
  VkDevice vk_device_;
  VkQueue vk_queue_;
  VkCommandPool vk_cmd_pool_;
  VkPhysicalDeviceProperties pdev_props_;
};

VulkanTest::VulkanTest() {
  struct VkApplicationInfo application_info = {};
  application_info.sType = VK_STRUCTURE_TYPE_APPLICATION_INFO;
  application_info.pApplicationName = "dual_quad_bench";
  application_info.applicationVersion = 1;
  application_info.pEngineName = "dual_quad_bench";
  application_info.engineVersion = 1;

  struct VkInstanceCreateInfo instance_create_info = {};
  instance_create_info.sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO;
  instance_create_info.pApplicationInfo = &application_info;

  VkResult result =
      vkCreateInstance(&instance_create_info, nullptr, &vk_instance_);
  if (result != VK_SUCCESS)
    throw - 1;

  std::uint32_t device_count = 1;
  result = vkEnumeratePhysicalDevices(vk_instance_, &device_count,
                                      &vk_physical_device_);
  if (result != VK_SUCCESS && result != VK_INCOMPLETE)
    throw - 1;

  if (device_count != 1) {
    fprintf(stderr, "device count: %d\n", device_count);
    throw - 1;
  }

  vkGetPhysicalDeviceProperties(vk_physical_device_, &pdev_props_);
  const float priorities[] = {0.5};
  struct VkDeviceQueueCreateInfo queue_info = {};
  queue_info.sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
  queue_info.queueFamilyIndex = 0;
  queue_info.queueCount = 1;
  queue_info.pQueuePriorities = priorities;
  struct VkDeviceCreateInfo device_create_info = {};
  device_create_info.sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO;
  device_create_info.queueCreateInfoCount = 1;
  device_create_info.pQueueCreateInfos = &queue_info;

  result = vkCreateDevice(vk_physical_device_, &device_create_info, nullptr,
                          &vk_device_);
  if (result != VK_SUCCESS)
    throw - 1;

  vkGetDeviceQueue(vk_device_, 0, 0, &vk_queue_);

  VkCommandPoolCreateInfo cmd_pool_create_info = {};
  cmd_pool_create_info.sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO;
  cmd_pool_create_info.flags = VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT;
  cmd_pool_create_info.queueFamilyIndex = 0;

  result = vkCreateCommandPool(vk_device_, &cmd_pool_create_info, nullptr,
                               &vk_cmd_pool_);
  if (result != VK_SUCCESS)
    throw - 1;
}

VulkanTest::~VulkanTest() {
  if (vk_cmd_pool_)
    vkDestroyCommandPool(vk_device_, vk_cmd_pool_, nullptr);
  if (vk_device_)
    vkDestroyDevice(vk_device_, nullptr);
  if (vk_instance_)
    vkDestroyInstance(vk_instance_, nullptr);
}

class DualQuadTest : public VulkanTest {
public:
  DualQuadTest();
  ~DualQuadTest();

  void run();

private:
  void setup_image();
  void setup_read_buffer();
  void setup_renderpass();
  void setup_framebuffer();
  void setup_pipeline();

  void record_cmd_buffer(int draws);

  void cleanup();

  VkFormat rb_format_;
  unsigned bytes_per_pixel_;
  std::uint32_t width_;
  std::uint32_t height_;

  VkRenderPass vk_renderpass_;
  VkFramebuffer vk_framebuffer_;

  VkImage image_;
  VkDeviceMemory image_memory_;
  VkImageView iview_;

  VkDeviceMemory read_memory_;
  VkBuffer read_buffer_;
  void *memory_map_;

  VkCommandBuffer run_cmd_buf_[2] = {VK_NULL_HANDLE, VK_NULL_HANDLE};

  VkPipeline pipeline_;
  VkPipelineLayout pipeline_layout_;

  VkFence fences_[2];

  VkQueryPool query_pool_;
};

DualQuadTest::DualQuadTest() {
  if (false) {
    rb_format_ = VK_FORMAT_R16G16B16A16_UNORM;
    bytes_per_pixel_ = 8;
  } else {
    rb_format_ = VK_FORMAT_R8G8B8A8_UNORM;
    bytes_per_pixel_ = 4;
  }
  width_ = 128;
  height_ = 128;

  setup_image();
  setup_read_buffer();
  setup_renderpass();
  setup_framebuffer();
  setup_pipeline();

  VkFenceCreateInfo fence_info = {};
  fence_info.sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO;
  fence_info.flags = VK_FENCE_CREATE_SIGNALED_BIT;

  auto result = vkCreateFence(vk_device(), &fence_info, nullptr, &fences_[0]);
  if (result != VK_SUCCESS)
    throw - 1;

  result = vkCreateFence(vk_device(), &fence_info, nullptr, &fences_[1]);
  if (result != VK_SUCCESS)
    throw - 1;

  VkQueryPoolCreateInfo pool_create_info = {};
  pool_create_info.sType = VK_STRUCTURE_TYPE_QUERY_POOL_CREATE_INFO;
  pool_create_info.queryType = VK_QUERY_TYPE_TIMESTAMP;
  pool_create_info.queryCount = 4;
  result =
      vkCreateQueryPool(vk_device(), &pool_create_info, NULL, &query_pool_);
  if (result != VK_SUCCESS)
    throw - 1;
}

DualQuadTest::~DualQuadTest() { cleanup(); }

void DualQuadTest::setup_image() {
  struct VkImageCreateInfo create_info = {};
  create_info.sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO;
  create_info.flags = 0; // TODO
  create_info.imageType = VK_IMAGE_TYPE_2D;
  create_info.format = rb_format_;
  create_info.extent = VkExtent3D{width_, height_, 1};
  create_info.mipLevels = 1;
  create_info.arrayLayers = 1;
  create_info.samples = VK_SAMPLE_COUNT_1_BIT;
  create_info.tiling = VK_IMAGE_TILING_OPTIMAL;
  create_info.usage = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT |
                      VK_IMAGE_USAGE_TRANSFER_SRC_BIT |
                      VK_IMAGE_USAGE_TRANSFER_DST_BIT;
  create_info.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
  create_info.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;

  auto result = vkCreateImage(vk_device(), &create_info, nullptr, &image_);
  if (result != VK_SUCCESS)
    throw - 1;

  VkMemoryRequirements reqs;
  vkGetImageMemoryRequirements(vk_device(), image_, &reqs);

  struct VkMemoryAllocateInfo alloc_info = {};
  alloc_info.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
  alloc_info.allocationSize = reqs.size;
  alloc_info.memoryTypeIndex = 2;

  result = vkAllocateMemory(vk_device(), &alloc_info, nullptr, &image_memory_);
  if (result != VK_SUCCESS)
    throw - 1;

  vkBindImageMemory(vk_device(), image_, image_memory_, 0);

  struct VkImageViewCreateInfo iview_info = {};
  iview_info.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
  iview_info.flags = 0;
  iview_info.image = image_;
  iview_info.viewType = VK_IMAGE_VIEW_TYPE_2D;
  iview_info.format = rb_format_;
  iview_info.components =
      (VkComponentMapping){VK_COMPONENT_SWIZZLE_R, VK_COMPONENT_SWIZZLE_G,
                           VK_COMPONENT_SWIZZLE_B, VK_COMPONENT_SWIZZLE_A};
  iview_info.subresourceRange =
      (VkImageSubresourceRange){VK_IMAGE_ASPECT_COLOR_BIT, 0, 1, 0, 1};
  result = vkCreateImageView(vk_device(), &iview_info, nullptr, &iview_);
  if (result != VK_SUCCESS)
    throw - 1;
}

void DualQuadTest::setup_read_buffer() {
  struct VkBufferCreateInfo create_info = {};
  create_info.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
  create_info.size = width_ * height_ * bytes_per_pixel_ * 4;
  create_info.usage = VK_BUFFER_USAGE_TRANSFER_DST_BIT;
  create_info.sharingMode = VK_SHARING_MODE_EXCLUSIVE;

  auto result =
      vkCreateBuffer(vk_device(), &create_info, nullptr, &read_buffer_);
  if (result != VK_SUCCESS)
    throw - 1;

  VkMemoryRequirements reqs;
  vkGetBufferMemoryRequirements(vk_device(), read_buffer_, &reqs);

  struct VkMemoryAllocateInfo alloc_info = {};
  alloc_info.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
  alloc_info.allocationSize = reqs.size;
  alloc_info.memoryTypeIndex = 2;

  result = vkAllocateMemory(vk_device(), &alloc_info, nullptr, &read_memory_);
  if (result != VK_SUCCESS)
    throw - 1;

  vkBindBufferMemory(vk_device(), read_buffer_, read_memory_, 0);

  result = vkMapMemory(vk_device(), read_memory_, 0, alloc_info.allocationSize,
                       0, &memory_map_);
  if (result != VK_SUCCESS)
    throw - 1;
}

void DualQuadTest::setup_renderpass() {
  struct VkAttachmentDescription attachment = {};
  attachment.format = rb_format_;
  attachment.samples = VK_SAMPLE_COUNT_1_BIT;
  attachment.loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
  attachment.storeOp = VK_ATTACHMENT_STORE_OP_STORE;
  attachment.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
  attachment.finalLayout = VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL;

  const VkAttachmentReference ref = {0,
                                     VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL};
  struct VkSubpassDescription subpass = {};
  subpass.pipelineBindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS;
  subpass.colorAttachmentCount = 1;
  subpass.pColorAttachments = &ref;

  struct VkRenderPassCreateInfo create_info = {};
  create_info.sType = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO;
  create_info.attachmentCount = 1;
  create_info.pAttachments = &attachment;
  create_info.subpassCount = 1;
  create_info.pSubpasses = &subpass;
  auto result =
      vkCreateRenderPass(vk_device(), &create_info, nullptr, &vk_renderpass_);
  if (result != VK_SUCCESS) {
    fprintf(stderr, "render pass create failed\n");
    throw - 1;
  }
}

void DualQuadTest::setup_framebuffer() {
  struct VkFramebufferCreateInfo create_info = {};
  create_info.sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO;
  create_info.flags = 0;
  create_info.renderPass = vk_renderpass_;
  create_info.attachmentCount = 1;
  create_info.pAttachments = &iview_;
  create_info.width = width_;
  create_info.height = height_;
  create_info.layers = 1;

  auto result =
      vkCreateFramebuffer(vk_device(), &create_info, nullptr, &vk_framebuffer_);
  if (result != VK_SUCCESS)
    throw - 1;
}

std::vector<char> load_shader(std::string const &filename) {
  std::ifstream in(filename.c_str());
  in.seekg(0, std::ios::end);
  std::vector<char> data(in.tellg());
  in.seekg(0, std::ios::beg);
  in.read(data.data(), data.size());
  return data;
}

void DualQuadTest::setup_pipeline() {

  struct VkPipelineLayoutCreateInfo layout_info = {};
  layout_info.sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO;

  auto result = vkCreatePipelineLayout(vk_device(), &layout_info, nullptr,
                                       &pipeline_layout_);
  if (result != VK_SUCCESS)
    throw - 1;

  auto frag_spv = load_shader("constant.frag.spv");
  auto vert_spv = load_shader("constant.vert.spv");
  if (frag_spv.size() % 4 || vert_spv.size() % 4)
    throw - 1;

  VkShaderModuleCreateInfo shader_info = {};
  shader_info.sType = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO;
  shader_info.codeSize = frag_spv.size();
  shader_info.pCode = (const uint32_t *)frag_spv.data();

  VkShaderModule vs_module, fs_module;
  result = vkCreateShaderModule(vk_device(), &shader_info, nullptr, &fs_module);
  if (result != VK_SUCCESS)
    throw - 1;

  shader_info.codeSize = vert_spv.size();
  shader_info.pCode = (const uint32_t *)vert_spv.data();
  result = vkCreateShaderModule(vk_device(), &shader_info, nullptr, &vs_module);
  if (result != VK_SUCCESS)
    throw - 1;

  VkPipelineShaderStageCreateInfo stages[2] = {};

  stages[0].sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
  stages[0].stage = VK_SHADER_STAGE_VERTEX_BIT;
  stages[0].module = vs_module;
  stages[0].pName = "main";

  stages[1].sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
  stages[1].stage = VK_SHADER_STAGE_FRAGMENT_BIT;
  stages[1].module = fs_module;
  stages[1].pName = "main";

  VkPipelineVertexInputStateCreateInfo vertex_input_state = {};
  vertex_input_state.sType =
      VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO;

  VkPipelineInputAssemblyStateCreateInfo input_assembly_state = {};
  input_assembly_state.sType =
      VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO;
  input_assembly_state.topology = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST;

  VkViewport viewport{0.0, 0.0, (float)width_, (float)height_, 0.0, 1.0};
  VkRect2D scissor_rect{{0, 0}, {width_, height_}};
  VkPipelineViewportStateCreateInfo viewport_state = {};
  viewport_state.sType = VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO;
  viewport_state.viewportCount = 1;
  viewport_state.pViewports = &viewport;
  viewport_state.scissorCount = 1;
  viewport_state.pScissors = &scissor_rect;

  VkPipelineRasterizationStateCreateInfo raster_state = {};
  raster_state.sType =
      VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO;
  raster_state.polygonMode = VK_POLYGON_MODE_FILL;
  raster_state.cullMode = VK_CULL_MODE_NONE;
  raster_state.frontFace = VK_FRONT_FACE_COUNTER_CLOCKWISE;

  VkPipelineColorBlendAttachmentState blend_attachment_state = {};
  blend_attachment_state.colorWriteMask =
      VK_COLOR_COMPONENT_R_BIT | VK_COLOR_COMPONENT_G_BIT |
      VK_COLOR_COMPONENT_B_BIT | VK_COLOR_COMPONENT_A_BIT;

  VkPipelineColorBlendStateCreateInfo blend_state = {};
  blend_state.sType = VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO;
  blend_state.attachmentCount = 1;
  blend_state.pAttachments = &blend_attachment_state;

  VkGraphicsPipelineCreateInfo pipeline_create_info = {};
  pipeline_create_info.sType = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO;
  pipeline_create_info.stageCount = 2;
  pipeline_create_info.pStages = stages;
  pipeline_create_info.pVertexInputState = &vertex_input_state;
  pipeline_create_info.pInputAssemblyState = &input_assembly_state;
  pipeline_create_info.pViewportState = &viewport_state;
  pipeline_create_info.pRasterizationState = &raster_state;
  pipeline_create_info.pColorBlendState = &blend_state;
  pipeline_create_info.renderPass = vk_renderpass_;
  pipeline_create_info.layout = pipeline_layout_;

  result = vkCreateGraphicsPipelines(
      vk_device(), nullptr, 1, &pipeline_create_info, nullptr, &pipeline_);
  if (result != VK_SUCCESS)
    throw - 1;
}

void DualQuadTest::record_cmd_buffer(int draws) {
  for (unsigned i = 0; i < 2; ++i) {
    if (run_cmd_buf_[i]) {
      vkResetCommandBuffer(run_cmd_buf_[i], 0);
    } else {
      struct VkCommandBufferAllocateInfo alloc_info = {};
      alloc_info.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
      alloc_info.commandPool = vk_cmd_pool();
      alloc_info.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
      alloc_info.commandBufferCount = 1;

      auto result =
          vkAllocateCommandBuffers(vk_device(), &alloc_info, &run_cmd_buf_[i]);
      if (result != VK_SUCCESS)
        throw - 1;
    }

    struct VkCommandBufferBeginInfo begin_info = {};
    begin_info.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
    auto result = vkBeginCommandBuffer(run_cmd_buf_[i], &begin_info);
    if (result != VK_SUCCESS)
      throw - 1;

    vkCmdResetQueryPool(run_cmd_buf_[i], query_pool_, 2 * i, 2);

    vkCmdWriteTimestamp(run_cmd_buf_[i], VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT,
                        query_pool_, 2 * i);

    VkClearValue clear_value = {};
    struct VkRenderPassBeginInfo rp_begin = {};
    rp_begin.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
    rp_begin.renderPass = vk_renderpass_;
    rp_begin.framebuffer = vk_framebuffer_;
    rp_begin.renderArea = {{0, 0}, {width_, height_}};
    rp_begin.clearValueCount = 1;
    rp_begin.pClearValues = &clear_value;

    clear_value.color.float32[0] = 1.0;
    clear_value.color.float32[1] = 0.0;
    clear_value.color.float32[2] = 0.0;
    clear_value.color.float32[3] = 1.0;

    vkCmdBeginRenderPass(run_cmd_buf_[i], &rp_begin,
                         VK_SUBPASS_CONTENTS_INLINE);

    vkCmdBindPipeline(run_cmd_buf_[i], VK_PIPELINE_BIND_POINT_GRAPHICS,
                      pipeline_);
    vkCmdDraw(run_cmd_buf_[i], 3 * draws, 1, 0, 0);

    vkCmdEndRenderPass(run_cmd_buf_[i]);

    VkImageMemoryBarrier barrier = {};
    barrier.srcAccessMask = VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;
    barrier.dstAccessMask = VK_ACCESS_TRANSFER_READ_BIT;
    barrier.oldLayout = VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL;
    barrier.newLayout = VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL;
    barrier.image = image_;
    barrier.subresourceRange = (VkImageSubresourceRange){
        .aspectMask = VK_IMAGE_ASPECT_COLOR_BIT,
        .baseMipLevel = 0,
        .levelCount = 1,
        .baseArrayLayer = 0,
        .layerCount = 1,
    };

    vkCmdPipelineBarrier(run_cmd_buf_[i], VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT,
                         VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT, 0, 0, NULL, 0,
                         NULL, 1, &barrier);

    vkCmdWriteTimestamp(run_cmd_buf_[i], VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT,
                        query_pool_, 2 * i + 1);

    VkBufferImageCopy region = {};
    region.bufferOffset = 0;
    region.bufferRowLength = width_;
    region.bufferImageHeight = height_;
    region.imageSubresource = {VK_IMAGE_ASPECT_COLOR_BIT, 0, 0, 1};
    region.imageOffset = {0, 0, 0};
    region.imageExtent = {width_, height_, 1};

    vkCmdCopyImageToBuffer(run_cmd_buf_[i], image_,
                           VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL, read_buffer_,
                           1, &region);

    result = vkEndCommandBuffer(run_cmd_buf_[i]);
    if (result != VK_SUCCESS)
      throw - 1;
  }
}

double get_current_time(void) {
  struct timespec tv;
  clock_gettime(CLOCK_MONOTONIC, &tv);
  return tv.tv_nsec / 1e9 + tv.tv_sec;
}

void DualQuadTest::run() {
  int draws = (1200000ull * 128 * 128) / width_ / height_;
  vkResetFences(vk_device(), 2, fences_);

  record_cmd_buffer(draws);

  double last_t = 0.0;
  for (int i = 0; i < 100; ++i) {
    int index = i & 1;
    double t_begin = get_current_time();
    struct VkSubmitInfo submit_info = {};
    submit_info.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
    submit_info.commandBufferCount = 1;
    submit_info.pCommandBuffers = &run_cmd_buf_[index];

    auto result = vkQueueSubmit(vk_queue(), 1, &submit_info, fences_[index]);
    if (result != VK_SUCCESS)
      throw - 1;

    if (i) {
      result = vkWaitForFences(vk_device(), 1, &fences_[1 - index], true,
                               10000000000ull);
      if (result == VK_TIMEOUT) {
        fprintf(stderr, "Timeout\n");
        throw - 1;
      } else if (result != VK_SUCCESS)
        throw - 1;

      double t_end = get_current_time();

      uint64_t timestamps[2];
      result = vkGetQueryPoolResults(
          vk_device(), query_pool_, (1 - index) * 2, 2, 16, timestamps, 8,
          VK_QUERY_RESULT_64_BIT | VK_QUERY_RESULT_WAIT_BIT);
      if (result != VK_SUCCESS)
        throw - 1;

      double timestamp_diff = (timestamps[1] - timestamps[0]) *
                              pdev_props().limits.timestampPeriod / 1e9;
      fprintf(stderr, "Submit took %f seconds, for %f Gpixels/second\n",
              timestamp_diff,
              (1.0 * width_ * height_ * draws) / timestamp_diff / 1e9);
      last_t = t_end;
      vkResetFences(vk_device(), 1, &fences_[1 - index]);
    }
  }
  std::ofstream out("dump.bin");
  out.write((const char *)memory_map_, width_ * height_ * bytes_per_pixel_);
}

void DualQuadTest::cleanup() {
  if (vk_renderpass_)
    vkDestroyRenderPass(vk_device(), vk_renderpass_, nullptr);
}

int main() {
  DualQuadTest test;
  test.run();
  return 0;
}
