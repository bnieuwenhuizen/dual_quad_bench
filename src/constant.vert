#version 450

#extension GL_ARB_separate_shader_objects : enable
#extension GL_ARB_shading_language_420pack : enable

out gl_PerVertex {
  vec4 gl_Position;
};


void main()
{
  if (gl_VertexIndex % 3 == 0)
    gl_Position = vec4(-1.0, -1.0, 0.0, 1.0);
  else if (gl_VertexIndex % 3 == 1)
    gl_Position = vec4(-1.0, 3.0, 0.0, 1.0);
  else
    gl_Position = vec4(3.0, -1.0, 0.0, 1.0);
}
